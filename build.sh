#!/bin/sh
set -e

export PATH=usr/lib/ccache/bin:$PATH
PLATFORM=`ruby -e "puts RbConfig::CONFIG['arch']"`

cd /home/builder

gem build ${GEM}.gemspec
gem compile -V --prune ${GEM}-${VERSION}.gem
gem inabox -o ${GEM}-${VERSION}-${PLATFORM}.gem \
  --host https://${HTTP_BASIC_USER}:${HTTP_BASIC_PASSWORD}@gems.${SUTTY}

cp *.gem /srv/gems/
