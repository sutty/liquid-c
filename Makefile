gem_dir := $(shell readlink -f ../gems)
include ../sutty/.env

SUTTY := sutty.nl
GEM := liquid-c
VERSION := 4.0.0

all: build build-gems

build:
	docker build -t sutty/liquid-c .

$(gem_dir)/$(GEM)-$(VERSION).gem:
	gem build $(GEM).gemspec
	mv $(notdir $@) $(dir $@)

$(gem_dir)/$(GEM)-$(VERSION)-x86_64-linux-musl.gem:
	docker run \
		-v $(gem_dir):/srv/gems \
		-v `readlink -f ~/.ccache`:/home/builder/.ccache \
		-e HTTP_BASIC_USER=$(HTTP_BASIC_USER) \
		-e HTTP_BASIC_PASSWORD=$(HTTP_BASIC_PASSWORD) \
		-e GEM=$(GEM) -e VERSION=$(VERSION) \
		sutty/liquid-c:latest

$(gem_dir)/$(GEM)-$(VERSION)-x86_64-linux.gem: $(gem_dir)/$(GEM)-$(VERSION).gem
	gem compile -V --prune $(GEM)-$(VERSION).gem
	gem inabox -o $(GEM)-$(VERSION)-x86_64-linux.gem \
		--host https://$(HTTP_BASIC_USER):$(HTTP_BASIC_PASSWORD)@gems.$(SUTTY)
	mv -v *.gem $(gem_dir)/

build-gems: $(gem_dir)/$(GEM)-$(VERSION)-x86_64-linux-musl.gem \
	$(gem_dir)/$(GEM)-$(VERSION)-x86_64-linux.gem
