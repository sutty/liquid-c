FROM sutty/gem-compiler:latest
MAINTAINER "f <f@sutty.nl>"

ENV GEM=liquid-c
ENV VERSION=4.0.0
ENV SUTTY=sutty.nl
ENV HTTP_BASIC_USER=sutty
ENV HTTP_BASIC_PASSWORD=gibberish
ENV NOKOGIRI_USE_SYSTEM_LIBRARIES=1

USER root
COPY ./build.sh /usr/local/bin/build
COPY . /home/builder
RUN chmod 755 /usr/local/bin/build
RUN chown -R builder:builder /home/builder
RUN chmod -R o=g /home/builder

USER builder
ENTRYPOINT /usr/local/bin/build
